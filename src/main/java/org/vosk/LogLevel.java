package org.vosk;

/**
 * 日志级别
 *
 * @author YZD
 */
public enum LogLevel {
    /**
     * Print warning and errors
     */
    WARNINGS(-1),
    /**
     * Print info, along with warning and error messages, but no debug
     */
    INFO(0),
    // Print debug info
    DEBUG(1);

    private final int value;

    LogLevel(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }
}
